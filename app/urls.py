from django.conf.urls import url, include
from django.contrib import admin
from django.conf import settings
from core.views import index, home
from django.conf.urls.static import static

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^i18n/', include('django.conf.urls.i18n')),
    url(r'^accounts/', include('registration.backends.simple.urls')),
    url(r'^$', index, name="index"),
    
]
