from django.views.generic import TemplateView
from microsaas.codes.views import CodeRedirectMixin

class IndexView(TemplateView):
    template_name="core/index.html"

index = IndexView.as_view()

class HomeView(CodeRedirectMixin, TemplateView):
    template_name="core/home.html"

home = HomeView.as_view()